# Free Software and Libre Agricultural Economics

------------------------------------

**Initial Blind Draft**
1.  Policy framework for Libre Agricultural economics at (from) the village level.

2.  Free Software ( & Hardware) Technology and Policies that suits Agriculture and the libre economics.
     1. Free Software as in sense of using Free Software tools for Community Agricultural Prodction practise and  Management.
     2. Free Hardware as in sense of exculsive use of modular and Free Software - Hardware for Agricultural Growing and Production Practises.
     3. Libre/Free Economics in a sense to have self control on Production, Managemnet and Distribution of goods with demonopolized, un-authorized, Open Community economic and self controlled sustainable policies.  

3.  Structural framework for involving Educted Youth (Interested candidates),
Contributing  their part-time as community educated farmer and their collaboration 
with Village women for partime community farming. This collaboration should eventually 
empowers village women and to generate collaborative income.

-----------------------------------------------



 -----------------------------------------------
 
 ## Resources and References:
 
1. ***The influence of Economics on agricultural systems: an evolutionary and ecological perspective*** K. Maréchal , H. Aubaret-Joachain, J-P. Ledant https://orbi.uliege.be/bitstream/2268/208733/1/CEB-WorkingPaper08-028.pdf
 
 ----------------------------------------------
 

### Further Updates to this Project is at [Project Wiki](https://gitlab.com/gorlapraveen/free-software-and-libre-agricultural-economics/wikis/home)
 
 
------------------------------------------------------------------------

Please see [License](/LICENSE), [Contrubuting Guidelines](/CONTRIBUTING.md) and [Code of Conduct](/CodeOfConduct.md)