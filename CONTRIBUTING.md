# Contributors:

As everyone has an equal right to access the information of there own choice, one should not have sole power/supremacy to restrict others in accessing the resources. At the same time, one should have an opportunity to contribute to the public domain.

So, We are very glad to have everyone on this board to access or/and contribute to this public repository.

----------------------------------

## Guidelines and Agreement  : 
* Any Individual/Group/Organization, who can demonstrate their desire for Free, Open Culture and Libre Economics.
* Areas of Contribution : 
  1. Free Software and Hardware Agricultural Production and Management practises/resources.
  2. Free/Libre Economic Polices and Resources : That refects the Agricultural Production, Managemnt with Open Community and self sustainable economic Guidelines .
  3. Any Other that respects the Theme and [ License section (1), (2) and (3)](/CONTRIBUTING.md#your-contributions-isare-licensed-under-the-following-guidelines)
* For Documenting, you can continue to write at [README.md](/README.md)(only Short Documentation) or else you can create a new file of your own choice in [docs](/docs) folder (Create one of not) while referencing them in [README.md](/README.md).
* For contribution related to software such as code or Hardware files, Please create scripts or folders(For Multiple Script pages) in [Code](/code) folder (create one if not) while referencing them in [README.md](/README.md).
### Your contribution(s) is/are **Licensed** under the following guidelines:
  1.  Documentation [includes text, links, images and videos] is licensed under [GNU Free Documentation License](https://www.gnu.org/licenses/licenses.html#FDL) or/and [Creative Commons Attribution 4.0 International (CC BY 4.0) ](https://creativecommons.org/licenses/by/4.0/legalcode) 
  2.  Code/Software is licensed under [GNU General Publice License 2+](https://www.gnu.org/licenses/licenses.html#GPL).
  3.  Any Work that is adapted from other Sources (including text/images/Software Code) should have already been Licensed under either [1](#) or/and [2](#).
  4.  Adaptations of Your work should include Resources with citation(a.k.a Referencing)
* Contributors must obey our [Code of Conduct](/CodeOfConduct.md)

**------ Copy, Share and Contribute! Happy! Thankyou!----------**

